// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getAuth, GoogleAuthProvider} from 'firebase/auth';
import { getStorage } from 'firebase/storage'

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyA_GW8i8H9Z0rJsLAbRSwvuDawvFQJ2bOQ",
  authDomain: "informationform-c694c.firebaseapp.com",
  projectId: "informationform-c694c",
  storageBucket: "informationform-c694c.appspot.com",
  messagingSenderId: "433365814594",
  appId: "1:433365814594:web:0b4255df18ec69de9f4cdb"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)
const auth = getAuth();
const provider=new GoogleAuthProvider();
const storage = getStorage(app);
export {db,auth,provider,storage}