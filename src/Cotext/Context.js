import { createContext, useState, useContext } from "react";

const LoaderContext = createContext();

import React from "react";

function LoaderContextProvider(props) {
  const [qrshow, setqrShow] = React.useState(false);
  const [update, setUpdate] = React.useState(false);
  const value = {
    qrshow,
    setqrShow,
    update,
    setUpdate,
  };
  return (
    <LoaderContext.Provider value={value}>
      {props.children}
    </LoaderContext.Provider>
  );
}

export const useQrandUpadateContext = () => useContext(LoaderContext);
export default LoaderContextProvider;
