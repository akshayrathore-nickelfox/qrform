import * as React from "react";
import { useRouter } from "next/router";
import { getSession } from "@session/cookie";
import ShowToast from "src/ShowToast/showToast";


export default function PublicLayout({ children }) {
  const { replace } = useRouter();
  const session = getSession("user-token");

  React.useEffect(() => {
    if (session) {
      replace("/user/dashboard", "/user/dashboard");
    }
  }, [session]);

  return (
    <>
      {children}
      <ShowToast />
    </>
  );
}
