import React from 'react';
import Style from '../../../styles/chat.module.css';
import { Button } from '@mui/material';

function Dashboard() {
  
  return (
    <div className={Style.mainContainer}>
      <Button variant="contained">Logout</Button>
      <h1 className={Style.RName}>Chat</h1>
      <span className={Style.chatArea}>chatting area</span>
      <label className={Style.ibContainer}>
      <input className={Style.input}/>
      <button className={Style.sendBtn}>send</button>
      </label>
    </div>
  )
}

export default Dashboard
