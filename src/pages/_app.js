import React from "react"
import "../styles/globals.css"
import { ThemeProvider, createTheme } from "@mui/material/styles"
import { useRouter } from "next/router"
import { Provider as ReduxProvider } from "react-redux"
import PublicLayout from "../layouts/publicLayout"
import PrivateLayout from "../layouts/privateLayout"
import { defaultTheme } from "../themes/defaultTheme"
import store from "../redux/store"
import LoaderContextProvider from "src/Cotext/Context.js"


function MyApp({ Component, pageProps }) {
  const currentTheme = createTheme(defaultTheme)
  const path = useRouter()

  const isPublic = path.asPath.includes("/toast/")
  const isPrivate = path.asPath.includes("/user/")


  const Wrapper = PublicLayout

  return (
    <ReduxProvider store={store}>
      <LoaderContextProvider>
        <ThemeProvider theme={currentTheme}>
          <Wrapper>
            <Component {...pageProps} />
          </Wrapper>
        </ThemeProvider>
      </LoaderContextProvider>
    </ReduxProvider>
  )
}

export default MyApp
