import style from "../../styles/form.module.css";
import React, { useEffect } from "react";
import FormikForm from "../../FormikComponents/FormikContainer";
import Qrcode from "qrcode.react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, db, storage } from "src/firebase/firebase";
import { doc, getDoc, updateDoc } from "firebase/firestore";
import AppDispatcher from "@redux/dispatchers/appDispatcher";
import { ref, uploadString, getDownloadURL } from "firebase/storage";
import { v4 } from "uuid";
import FormNavbar from "src/Components/FormComponents/formNavbar";
import Signin from "src/FormikComponents/signin";
import UpdateFormContainer from "src/FormikComponents/UpdateFormContainer";
import { useQrandUpadateContext } from "src/Cotext/Context";

function Qrform() {
  const [user] = useAuthState(auth);
  const [qrvalue, setqrvalue] = React.useState("");
  const { qrshow, setqrShow, update } = useQrandUpadateContext();
  const [dataid, setDataid] = React.useState("");

  const getqrData = (values) => {
    setqrvalue(JSON.stringify(values));
  };

  const uploadQr = () => {
    const uuid = v4();
    const canvas = document.getElementById("qr");
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/jpeg");
    updateDoc(doc(db, "formData", dataid), {
      id: uuid,
    });
    //save image to storage
    const storageRef = ref(storage, `Qrimages/${uuid}`);
    uploadString(storageRef, pngUrl, "data_url").then((snap) => {
      getDownloadURL(ref(storage, `Qrimages/${snap.metadata.name}`))
        .then((url) => {
          updateDoc(doc(db, "formData", dataid), {
            url: url,
          });
        })
        .catch((error) => {
          console.log(error);
        });
    });

    AppDispatcher.showtoster("uploaded Successfully", "info");
  };

  const dwnQr = () => {
    const canvas = document.getElementById("qr");
    const pngUrl = canvas
      .toDataURL("image/png")
      .replace("image/png", "image/jpeg");
    let downloadLink = document.createElement("a");
    downloadLink.href = pngUrl;
    downloadLink.download = "qrcode.jpeg";
    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
    setqrShow(false);
    uploadQr();
  };

  useEffect(() => {
    const canvas = document.getElementById("qr");
    if (canvas && dataid && qrvalue) {
      const uuid = v4();
      const pngUrl = canvas
        .toDataURL("image/png")
        .replace("image/png", "image/jpeg");

      //save image to storage

      getDoc(doc(db, "formData", dataid))
        .then((dataSnap) => {
          let id = dataSnap.data().id;
          const storageRef = id
            ? ref(storage, `Qrimages/${id}`)
            : (updateDoc(doc(db, "formData", dataid), {
                id: uuid,
              }),
              ref(storage, `Qrimages/${uuid}`));

          uploadString(storageRef, pngUrl, "data_url").then((snap) => {
            getDownloadURL(
              ref(storage, `Qrimages/${id ? id : snap.metadata.name}`)
            )
              .then((url) => {
                updateDoc(doc(db, "formData", dataid), {
                  url: url,
                });
              })
              .catch((error) => {
                console.log(error);
              });
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [dataid, qrvalue]);

  return (
    <FormNavbar user={user}>
      {user ? (
        <>
          {!qrshow ? (
            <>
              {!update ? (
                <div className={style.form}>
                  <h2>Initial Form</h2>
                  <FormikForm
                    getqrData={getqrData}
                    setqrShow={setqrShow}
                    setDataid={setDataid}
                  />
                </div>
              ) : (
                <UpdateFormContainer
                  setqrShow={setqrShow}
                  getqrData={getqrData}
                  setDataid={setDataid}
                />
              )}
            </>
          ) : null}
          {qrshow ? (
            <>
              <div style={{ marginTop: "5pc" }}>
                <Qrcode id="qr" value={qrvalue} size={300} />
                <div style={{ display: "flex", justifyContent:'center' }}>
                  <button className={style.button} onClick={dwnQr}>
                    Download QR
                  </button>
                  <button className={style.button} onClick={uploadQr}>
                    Upload Qr
                  </button>
                </div>
              </div>
            </>
          ) : null}
        </>
      ) : (
        <>
          <div>
            <h2>Please Login First for any Action</h2>
            <Signin>
              Sign in With
              <br /> Google
            </Signin>
          </div>
        </>
      )}
    </FormNavbar>
  );
}

export default Qrform;
