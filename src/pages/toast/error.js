import React from "react";
import { Button, Container } from "src/Components/ToastComponents/button";
import AppDispatcher from "@redux/dispatchers/appDispatcher";
import ToastNavbar from "src/Components/ToastComponents/toastNavbar";

function Error() {
  const showToast = () => {
    //showtoster (msg,status)
    AppDispatcher.showtoster("error message popUp", "error");
  };

  return (
    <>
      <ToastNavbar />
      <Container>
        <Button color="red" onClick={showToast}>
          Error
        </Button>
      </Container>
    </>
  );
}

export default Error;
