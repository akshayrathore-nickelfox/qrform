import React from "react";
import { Button, Container } from "src/Components/ToastComponents/button";
import AppDispatcher from "@redux/dispatchers/appDispatcher";
import ToastNavbar from "src/Components/ToastComponents/toastNavbar";

function Warning() {
  const showToast = () => {
    //showtoster (msg,status)
    AppDispatcher.showtoster("hello", "success");
  };

  return (
    <>
      <ToastNavbar />
      <Container>
        <Button color="#d56936" onClick={showToast}>
          Warning
        </Button>
      </Container>
    </>
  );
}

export default Warning;
