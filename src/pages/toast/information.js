import React from "react";
import { Button, Container } from "src/Components/ToastComponents/button";
import AppDispatcher from "@redux/dispatchers/appDispatcher";
import ToastNavbar from "src/Components/ToastComponents/toastNavbar";

function Information() {
  const showToast = () => {
    //showtoster (msg,status)
    AppDispatcher.showtoster("info message popUp", "info");
  };

  return (
    <>
      <ToastNavbar />
      <Container>
        <Button color="#6c6bb1" onClick={showToast}>
          Information
        </Button>
      </Container>
    </>
  );
}

export default Information;
