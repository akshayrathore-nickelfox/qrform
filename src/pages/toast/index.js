import AppDispatcher from "@redux/dispatchers/appDispatcher";
import React from "react";
import { Button, Container } from "src/Components/ToastComponents/button";
import ToastNavbar from "src/Components/ToastComponents/toastNavbar";

function Toasters() {
  const showToast = () => {
    //showtoster (msg,status)
    AppDispatcher.showtoster("Success message popup", "success");
  };
  return (
    <>
      <ToastNavbar />
      <Container>
        <Button color="green" onClick={showToast}>
          Success
        </Button>
      </Container>
    </>
  );
}

export default Toasters;
