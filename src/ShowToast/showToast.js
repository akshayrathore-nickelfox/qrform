import React from "react";
import Toaster from "src/Components/ToastComponents/toaster";
import { useSelector } from "react-redux";
import { TostDiv } from "src/Components/ToastComponents/button";

function ShowToast() {
  const toastData = useSelector((state) => state.TostReducer.data);

  return (
    <TostDiv>
      {toastData
        ? toastData.map((obj) => {
            return (
              <Toaster
                key={Math.random() + 2}
                id={obj.status}
                show={obj.showToast}
                status={obj.status}
                msg={obj.msg}
              />
            );
          })
        : null}
    </TostDiv>
  );
}

export default ShowToast;
