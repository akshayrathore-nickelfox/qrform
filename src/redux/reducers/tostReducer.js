
const initialState = {
    data: []
};
const TostReducer = (state = initialState, action) => {
    switch (action.type) {
        case "show": return {
            ...state,
            data: [
            {
                showToast: true,
                msg: action.msg,
                status: action.state
            },...state.data
            ]
        };
        case "hide": {
            state.data.map(obj=>{
                if(obj.status === action.state){
                    state.data.splice(state.data.indexOf(obj),1)
                }
            })
            return{
                ...state,
                data:[...state.data]
            }
        }
        default: return state;
    }
}

export default TostReducer