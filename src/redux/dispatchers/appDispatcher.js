// App wide dispatcher

import Stores from "../store";
import Actions from "../actions/appActions";
import  PopUp from "../actions/tostAction";

const AppDispatcher = {
  setUserLoggedIn: (data) => {
    Stores.dispatch({ type: Actions.SET_TOKEN, data: data.tokens });
    Stores.dispatch({ type: Actions.LOGIN, data: data.user });
  },
  setUserLoggedOut: () => {
    Stores.dispatch({ type: Actions.LOGOUT });
  },
  updateUserTokens: (data) => {
    Stores.dispatch({ type: Actions.SET_TOKEN, data });
  },
  updateUserInfo: (data) => {
    Stores.dispatch({ type: Actions.LOGIN, data });
  },
  showtoster:(msg,state)=>{
    Stores.dispatch({type: PopUp.show,msg,state})
  },
  hidetoster:(state)=>{
    Stores.dispatch({type: PopUp.hide,state})
  }

};

export default AppDispatcher;
