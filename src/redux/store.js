import { createStore, combineReducers } from "redux";
import AppReducer from "./reducers/appReducer";
import TostReducer  from "./reducers/tostReducer";

const AllReducer = {
  App:AppReducer,
  TostReducer
  
};

const rootReducer = combineReducers(AllReducer);

const store = createStore(rootReducer);
export default store;
