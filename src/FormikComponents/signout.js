import React from 'react';
import { auth } from 'src/firebase/firebase';
import style from "../styles/formNav.module.css";


function Signout(props) {

    const signOut=()=>{
        auth.signOut()
        console.log("logout");
    }
  return (

    <button className={style.signOut} onClick={signOut}>{props.children}</button>
  )
}

export default Signout