import { GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import React from "react";
import { auth, provider } from "src/firebase/firebase";
import style from "../styles/formNav.module.css";

function Signin(props) {
  

  const signinWithGoogle = () => {
    signInWithPopup(auth, provider)
      .then((result) => {
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential.accessToken;
        const user = result.user;
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        const email = error.email;
        const credential = GoogleAuthProvider.credentialFromError(error);
        console.log({ errorCode, errorMessage, email, credential });
      });
  };

  return <button className={style.signinBtn} onClick={signinWithGoogle}>{props.children}</button>;
}

export default Signin;
