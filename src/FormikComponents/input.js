import { ErrorMessage, Field } from "formik";
import React from "react";
import TextError from "src/Components/ToastComponents/textError";
import style from "../styles/form.module.css";

function Input(props) {
  const { name, label, ...rest } = props;
  return (
    <>
      <ErrorMessage name={name} component={TextError} />
      <div className={style.inputForm}>
        <label htmlFor={name}>{label}</label>
        <Field id={name} name={name} {...rest} />
      </div>
    </>
  );
}

export default Input;
