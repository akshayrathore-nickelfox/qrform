import {
  collection,
  getDocs,
  setDoc,
  doc,
  getDoc,
  deleteDoc,
  updateDoc,
} from "firebase/firestore";
import { ref, getDownloadURL } from "firebase/storage";
import React, { useEffect, useState } from "react";
import { db, storage } from "src/firebase/firebase";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import style from "../styles/form.module.css";
import EditIcon from "@mui/icons-material/Edit";
import { ErrorMessage, Field, Form, Formik } from "formik";
import * as Yup from "yup";
import TextError from "./TextError";

const initialValues = {
  name: "",
  email: "",
  phone: "",
  salary: "",
};

const validationSchema = Yup.object({
  name: Yup.string().required("Required"),
  email: Yup.string().email("Not Valid Email").required("Required"),
  phone: Yup.string().required("Required").min(10).max(12),
  salary: Yup.string().min(5).required("Required"),
});

function UpdateFormContainer(props) {
  let updateData = "";
  const [showinput, setShowInput] = useState({});
  const [updateQrData, setUpdateQRdata] = useState(false);
  const [dataArray, setDataArray] = useState([]);
  const [qrsrc, setQrSrc] = useState();
  const [view, setView] = useState(false);

  const handleEdit = (id, name, index) => {
    setShowInput({
      id,
      name,
    });
    initialValues[name] = dataArray[index][name];
  };

  const handleUpdate = (id, key) => {
    if (key === "email") {
      const docref = doc(db, "formData", updateData);
      getDoc(doc(db, "formData", id))
        .then((dataSnap) => {
          setDoc(docref, {
            ...dataSnap.data(),
            email: updateData,
          }).then(() => {
            deleteDoc(doc(db, "formData", id));
            props.setDataid(updateData);
            getDoc(doc(db, "formData", updateData))
              .then((dataSnap) => {
                let newData = JSON.stringify({
                  name: dataSnap.data().name,
                  email: dataSnap.data().email,
                  phone: dataSnap.data().phone,
                  salary: dataSnap.data().salary,
                });
                setUpdateQRdata(newData);
                props.setqrShow(true);
                setTimeout(() => {
                  props.setqrShow(false);
                }, 0);
              })
              .catch((err) => console.log(err));
          });
        })
        .catch((err) => console.log(err));
    } else {
      const docref = doc(db, "formData", id);
      updateDoc(docref, {
        [key]: updateData,
      }).then(() => {
        props.setDataid(id);
        getDoc(doc(db, "formData", id))
          .then((dataSnap) => {
            let newData = JSON.stringify({
              name: dataSnap.data().name,
              email: dataSnap.data().email,
              phone: dataSnap.data().phone,
              salary: dataSnap.data().salary,
            });
            setUpdateQRdata(newData);
            props.setqrShow(true);
            setTimeout(() => {
              props.setqrShow(false);
            }, 0);
          })
          .catch((err) => console.log(err));
      });
      setShowInput("");
    }
  };

  const dwnUpdatedQr = (id) => {
    getDownloadURL(ref(storage, `Qrimages/${id}`))
      .then((url) => {
        let downloadLink = document.createElement("a");
        downloadLink.href = url;
        downloadLink.download = "qrcode.jpeg";
        downloadLink.target = "_blank";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      })
      .catch((error) => console.log(error));
  };
  const viewQr = (id) => {
    setView(true);
    setQrSrc("");
    getDownloadURL(ref(storage, `Qrimages/${id}`))
      .then((url) => {
        setQrSrc(url);
      })
      .catch((error) => console.log(error));
  };

  useEffect(() => {
    const colRef = collection(db, "formData");
    const data = [];
    getDocs(colRef)
      .then((dataSnap) => {
        dataSnap.forEach((doc) => {
          data.push(doc.data());
        });
        setDataArray([...data]);
      })
      .catch((err) => {
        console.log(err);
      });
    props.getqrData(updateQrData);
  }, [updateQrData]);

  return (
    <div className="Table" style={{ margin: "1pc" }}>
      {view ? (
        <div style={{ position: "relative" }}>
          {qrsrc ? <img src={qrsrc} alt="qrimg" /> : <h1>Loading QR...</h1>}
          <button
            className={style.updateDwnBtn}
            style={{
              display: "block",
              backgroundColor: "red",
              margin: "1pc auto",
            }}
            onClick={() => {
              setView(false);
            }}
          >
            Close Here
          </button>
        </div>
      ) : dataArray.length ? (
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell align="center">
                  <h2>Name</h2>
                </TableCell>
                <TableCell align="center">
                  <h2>Email</h2>
                </TableCell>
                <TableCell align="center">
                  <h2>phone No</h2>
                </TableCell>
                <TableCell align="center">
                  <h2>Salary</h2>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {dataArray.map((obj, index) => (
                <TableRow
                  key={obj.email}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell align="center">
                    {showinput.id === obj.email && showinput.name === "name" ? (
                      <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                      >
                        {(formik) => (
                          <Form
                            className={style.update}
                            onSubmit={(e) => {
                              e.preventDefault();
                              updateData = e.target.name.value;
                              handleUpdate(obj.email, "name");
                            }}
                          >
                            <ErrorMessage name="name" component={TextError} />
                            <div>
                              <Field id={obj.email} name="name" />
                              <button
                                type="submit"
                                disabled={formik.errors.name ? true : false}
                              >
                                Update
                              </button>
                            </div>
                          </Form>
                        )}
                      </Formik>
                    ) : (
                      <div
                        className={style.tableData}
                        onClick={() => handleEdit(obj.email, "name", index)}
                      >
                        {obj.name} <EditIcon fontSize="" />
                      </div>
                    )}
                  </TableCell>
                  <TableCell align="center">
                    {showinput.id === obj.email &&
                    showinput.name === "email" ? (
                      <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                      >
                        {(formik) => (
                          <Form
                            className={style.update}
                            onSubmit={(e) => {
                              e.preventDefault();
                              updateData = e.target.email.value;
                              handleUpdate(obj.email, "email");
                            }}
                          >
                            <ErrorMessage name="email" component={TextError} />
                            <div>
                              <Field id={obj.email} name="email" type="email" />
                              <button
                                type="submit"
                                disabled={formik.errors.email ? true : false}
                              >
                                Update
                              </button>
                            </div>
                          </Form>
                        )}
                      </Formik>
                    ) : (
                      <div
                        className={style.tableData}
                        onClick={() => handleEdit(obj.email, "email", index)}
                      >
                        {obj.email} <EditIcon fontSize="" />
                      </div>
                    )}
                  </TableCell>
                  <TableCell align="center">
                    {showinput.id === obj.email &&
                    showinput.name === "phone" ? (
                      <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                      >
                        {(formik) => (
                          <Form
                            className={style.update}
                            onSubmit={(e) => {
                              e.preventDefault();
                              updateData = e.target.phone.value;
                              handleUpdate(obj.email, "phone");
                            }}
                          >
                            <ErrorMessage name="phone" component={TextError} />
                            <div>
                              <Field
                                id={obj.email}
                                name="phone"
                                type="number"
                              />
                              <button
                                type="submit"
                                disabled={formik.errors.phone ? true : false}
                              >
                                Update
                              </button>
                            </div>
                          </Form>
                        )}
                      </Formik>
                    ) : (
                      <div
                        className={style.tableData}
                        onClick={() => handleEdit(obj.email, "phone", index)}
                      >
                        {obj.phone} <EditIcon fontSize="" />
                      </div>
                    )}
                  </TableCell>
                  <TableCell align="center">
                    {showinput.id === obj.email &&
                    showinput.name === "salary" ? (
                      <Formik
                        initialValues={initialValues}
                        validationSchema={validationSchema}
                      >
                        {(formik) => (
                          <Form
                            className={style.update}
                            onSubmit={(e) => {
                              e.preventDefault();
                              updateData = e.target.salary.value;
                              handleUpdate(obj.email, "salary");
                            }}
                          >
                            <ErrorMessage name="salary" component={TextError} />
                            <div>
                              <Field
                                id={obj.email}
                                name="salary"
                                type="number"
                              />
                              <button
                                type="submit"
                                disabled={formik.errors.salary ? true : false}
                              >
                                Update
                              </button>
                            </div>
                          </Form>
                        )}
                      </Formik>
                    ) : (
                      <div
                        className={style.tableData}
                        onClick={() => handleEdit(obj.email, "salary", index)}
                      >
                        {obj.salary} <EditIcon fontSize="" />
                      </div>
                    )}
                  </TableCell>
                  <TableCell align="right">
                    <button
                      className={style.updateDwnBtn}
                      onClick={() => {
                        viewQr(obj.id);
                      }}
                    >
                      View Qr
                    </button>
                    <button
                      className={style.updateDwnBtn}
                      onClick={() => {
                        dwnUpdatedQr(obj.id);
                      }}
                    >
                      Download Qr
                    </button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      ) : (
        <h1>No Data Available</h1>
      )}
    </div>
  );
}

export default UpdateFormContainer;
