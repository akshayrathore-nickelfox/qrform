import React, { useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormikControl from "./FormikControl";
import AppDispatcher from "@redux/dispatchers/appDispatcher";
import {
  collection,
  doc,
  getDocs,
  query,
  setDoc,
  where,
} from "firebase/firestore";

import { db } from "src/firebase/firebase";

const initialValues = {
  name: "",
  email: "",
  phone: "",
  salary: "",
};

const validationSchema = Yup.object({
  name: Yup.string().required("required"),
  email: Yup.string().email("Invaild Format").required("required"),
  phone: Yup.string().min(10).max(12).required("required"),
  salary: Yup.string().min(5).required("required"),
});

export default function FormikContainer(props) {
  const colref = collection(db, "formData");

  const [loader, setloader] = useState(false);
  if (props.initialValues) {
    initialValues = props.initialValues;
  }

  const onSubmit = (values, { resetForm }) => {
    setloader(true);
    const docref = doc(db, "formData", `${values.email}`);
    const q = query(colref, where("email", "==", values.email));
    getDocs(q).then((snapshot) =>
      snapshot.docs.length
        ? (AppDispatcher.showtoster("Email already Exist", "error"),
          setloader(false))
        : setDoc(docref, {
            ...values,
          }).then(() => {
            setloader(false);
            props.setqrShow(true);
            props.getqrData(values);
            props.setDataid(values.email);
            AppDispatcher.showtoster("Qr Generated", "success");
          })
    );
    resetForm(initialValues);
  };

  return loader ? (
    <h1>Qr Loading.....</h1>
  ) : (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {(formik) => (
          <Form id="form">
            <FormikControl
              control="input"
              type="text"
              label="Name"
              name="name"
            />
            <hr />

            <FormikControl
              control="input"
              type="email"
              label="Email"
              name="email"
            />
            <hr />
            <FormikControl
              control="input"
              type="number"
              label="Phone No"
              name="phone"
            />
            <hr />
            <FormikControl
              control="input"
              type="number"
              label="Salary"
              name="salary"
            />
            <hr />
            <button type="submit">Generate Qr Code</button>
          </Form>
        )}
      </Formik>
    </>
  );
}
