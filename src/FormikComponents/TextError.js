import React from "react";
import style from "../styles/form.module.css";

function TextError(props) {
  return <div className={style.errorMsg}>{props.children}</div>;
}

export default TextError;
