import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import AutofpsSelectIcon from "@mui/icons-material/AutofpsSelect";
import { makeStyles } from "@mui/styles";
import Link from "next/link";

const useStyles = makeStyles({
  Typography: {
    backgroundColor: "green",
    margin: "0 auto",
    fontSize: "1.5pc",
  },
});

function MainNavbar() {
  const cls = useStyles();

  return (
    <AppBar>
      <Toolbar>
        <AutofpsSelectIcon />
        <Typography varient="h6" className={cls.Typography}>
          Assignments
        </Typography>
        <Link href="/formik/qrform" passHref>
          <li style={{cursor:"pointer"}}>QrForm</li>
        </Link>
      </Toolbar>
    </AppBar>
  );
}

export default MainNavbar;
