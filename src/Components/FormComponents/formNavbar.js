import React from "react";
import style from "../../styles/formNav.module.css";
import AssignmentIcon from "@mui/icons-material/Assignment";
import Signin from "src/FormikComponents/signin";
import Signout from "src/FormikComponents/signout";
import { useQrandUpadateContext } from "../../Cotext/Context";
import image from "../../../public/formimg.jpg";
import Image from "next/image";

function FormNavbar({ user, children }) {
  const { qrshow, setqrShow, update, setUpdate } = useQrandUpadateContext();
  return (
    <>
      <nav className={style.nav}>
        <div>
          <h3>Information-Application</h3>
          <p>Generate information Qr code here</p>
        </div>
      </nav>
      <div className={style.navigate}>
        <AssignmentIcon sx={{ fontSize: 40 }} />
        <ul>
          <button
            className={
              qrshow || update
                ? style.button
                : `${style.button} ${style.buttonclicked}`
            }
            onClick={() => {
              setqrShow(false);
              setUpdate(false);
            }}
          >
            Initial Form
          </button>
          <button
            className={
              !update ? style.button : `${style.button} ${style.buttonclicked}`
            }
            onClick={() => {
              setqrShow(false);
              setUpdate(true);
            }}
          >
            Update Form
          </button>
        </ul>
        <div className={style.naviagtionbtn}>
          {!user ? (
            <Signin>Sign in With Google</Signin>
          ) : (
            <Signout>LogOut</Signout>
          )}
        </div>
      </div>
      <div className={!update ? style.children : style.childrenflex}>
        {!update && user && !qrshow ? (
          <div style={{ margin: "auto 0" }}>
            <Image src={image} alt="image" width="500" height="450" />
          </div>
        ) : null}

        <div> {children}</div>
      </div>
    </>
  );
}

export default FormNavbar;
