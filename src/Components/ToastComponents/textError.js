import React, { Children } from "react";
import style from "../../styles/form.module.css";

function TextError(props) {
  return (
    <div className={style.errorMsg}>
      <li>{props.children}</li>
    </div>
  );
}

export default TextError;
