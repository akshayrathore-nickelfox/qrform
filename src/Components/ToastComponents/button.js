import styled from '@emotion/styled'

export const Container = styled.div` 
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 86vh;
  background-color: #fff;
`
export const Button = styled.button`
   background-color: ${props => props.color};
   transition: background-color 0.3s ease-in;
   padding: 2pc;
   width: 15pc;
   box-shadow: 0px 1px 5px green;
   text-shadow: 0px 1px 5px black;
   letter-spacing: 1px;
   border-radius: 3px;
   color: white;
   font-size: 20px;
   font-weight: 600;
   cursor: pointer;
`
export const Toast = styled(Container)`
 background-color:  ${props =>props.status==="warning"?"rgba(213,105,54,0.80)"
                      :props.status==="info"?'rgba(108,107,177,0.78)'
                      : props.status==="error"?'rgba(255,0,0,0.69)'
                      :"rgba(100,205,23,0.74)"};
 position: relative;
 min-height: 80px;
 width:max-content;
 margin-top: 1.3pc;
 font-size: 1.2pc;
 font-weight: 500;
 padding-right: 5pc;
 padding-left: 10px;
 box-shadow: 0px 0px 5px black;
 color:white;
 visibility: ${props => !props.boolean ? "hidden" : "visible"}

`
export const Close = styled(Button)`
 position: absolute;
 right: 1pc ;
 top: 1pc;
 width: auto;
 padding: 8px;
 height: auto;
 background-color: red;
`
export const Symbol = styled.img`
  src: ${props => props.img};
` 
export const Text = styled.div`
 margin: 1pc;
`
export const TostDiv =styled.div`
  position: absolute;
  top:30%;
  left:0%;
  max-width: fit-content;
  max-height: fit-content;
`