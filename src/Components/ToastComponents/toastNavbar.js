import React from "react";
import Link from "next/link";
import style from "../../styles/toster.module.css";

function ToastNavbar() {
  return (
    <div className={style.tostContainer}>
      <nav>
        <ul>
          <Link href="/toast" passHref>
            <li>Success</li>
          </Link>
          <Link href="/toast/warning" passHref>
            <li>Warning</li>
          </Link>
          <Link href="/toast/error" passHref>
            <li>Error</li>
          </Link>
          <Link href="/toast/information" passHref>
            <li>Information</li>
          </Link>
        </ul>
      </nav>
    </div>
  );
}

export default ToastNavbar;
