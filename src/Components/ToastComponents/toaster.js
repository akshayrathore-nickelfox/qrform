import React, { useEffect, useRef } from 'react';
import { Toast, Text, Close } from './button';
import success from '../../../public/checked.png';
import warning from '../../../public/warning.jpg';
import error from '../../../public/error.jpg';
import info from '../../../public/information.jpg';
import Image from 'next/image';
import AppDispatcher from '@redux/dispatchers/appDispatcher';

function Toaster(props) {

  let image = props.status === "success" ? success
    : props.status === "warning" ? warning
      : props.status === "error" ? error
        : props.status === "info" ? info : success

  const hideToast = (e) => {
    AppDispatcher.hidetoster(e.target.id)
  }

  useEffect(() => {
    let time = setTimeout(() => {
      AppDispatcher.hidetoster(props.status)
    }, 4000)
    return () => {
      clearTimeout(time)
    }
  }, [props.status])

  return (
    <Toast boolean={props.show} status={props.status} {...props} >
      <Image src={image} alt='image' width={30} height={30} />
      <Text>{props.msg}</Text>
      <Close id={props.status} onClick={hideToast} >x</Close>
    </Toast>
  )
}

export default Toaster;